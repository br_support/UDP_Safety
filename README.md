# Jak nastavit komunikaci SafeLOGIC-SafeLOGIC přes UDP Tunnel pro AS V4.5
## Nastavení komunikace openSAFETYoverUDPTunnel
V Automation Studiu jsou vytvořeny 2 konfigurace. V každé konfiguraci je přidáno PLC připojené k SafeLOGIC. V tomto případě se jedná o konfigurace **1585** a **1586**  
  
![](images/Konfigurace_A.png)
![](images/Konfigurace_B.png)  
  
Obě PLC jsou nakonfigurovány tak, aby spolu komunikovaly přes Ethernet.  
  
![](images/IPEthernet_A.png)  
<img src="images/IPEthernet_B.png"  width="778">  
  
**openSAFETY over UDP Tunnel** musí být povoleno v obou konfiguracích (viz níže).  
  
Konfigurace **1585** i **1586**:  
  
![](images/UDP_povoleno.png)  
  
Na obou PLC je do Ethernet rozhraní je přidáno "openSAFETYoverUDPTunnel" a zde je nutno nastavit IP adresu PLC, se kterým má dané PLC komunikovat
(v konfiguraci **1585** je na **X20CP1585** v Ethernet konfiguraci nastavena IP adresa **192.168.0.2**, **ale v openSAFETYoverUDPTunnel konfiguraci je potřeba nastavit IP adresu PLC v druhé konfiguraci
X20CP1586 (tedy 192.168.0.3)**).  
<img src="images/KonfiguraceUDPA.PNG">  
  
To stejné provedeme v konfiguraci **1586**  
  
<img src="images/KonfiguraceUDPB.PNG">  
  
Také zde musí být přiřazeno pro každou konfiguraci openSAFETY over UDP jméno druhé konfigurace, se kterou komunikuje.  
  
Konfigurace **1586**:  
<img src="images/NazevA.PNG">  
Konfigurace **1585**:  
<img src="images/NazevB.PNG">  

## Nastavení komunikace SafeLOGIC-SafeLOGIC 
Pro tuto komunikaci je potřeba nastavit jeden SafeLOGIC jako **source**. V tomto případě je určen jako **source X20cSL8100** v konfiguraci **1586**.    
V konfiguraci pro X20cSL8100 je potřeba nastavit **Use as source SafeLOGIC** - on. Zde bylo též nastaveno **Extended source Safelogic communication** - on.
  
![](images/SourceSL.png)
  
V konfiguraci **1585** je následně v **X20SL8101 konfiguraci** nastavena komunikace s source SL.  
V **SafeLOGIC ID connection 1** je potřeba nastavit **SafeLOGIC ID** source SL (v tomto případě je ID pro X20xSL8100 - **1**)  
  
![](images/IDofSL.png)

## **DŮLEŽITÁ POZNÁMKA**
V aplikaci pro source SL je potřeba v SafeDESIGNERu je potřeba navýšit **Safe Data Duration**. Původně nastavených 20000 je pro tuto komunikaci málo.   
Heslo pro otevření obou aplikací (**SafeLOGIC-1** a **proj1**) v safeDESIGNERu je **123456** 
  
![](images/SafeDataDuration.png)